# Weaveability Library
A library repository for JavaScript code including but not exclusive to code relating to ReactJs and AWS services.

> Please create your own branch for working within the library repo. The Master Branch will be update during a group review.

## Structure
All library packages should be stored within their own folder within the `src` folder. The package folders should be named with their corrosponding file name in dash seperated format. The `default export` should be named in upper camel case.
```
import DaySkip from '../day-skip/day-skip.js'
```
> Folder should include a `.test.js` for unit testing as well as a `.mocks.js` and `.config.js` for ny mock data or configurations

## Usage
Remember to install any node dependencies to the project using `npm install`.
```
$ npm install
```

## Testing
The repo is configured to use [Jest](https://facebook.github.io/jest/) for automated unit testing. [Jest](https://facebook.github.io/jest/) will scan all folders within the `src` directory for any `.test.js` files and begin the automated testing process upon the following `npm test` command.
```
$ npm test
```
The repo is configured to run the [Quokka](https://quokkajs.com/) continous integration testing plugin for `VSCode`.

## Packaging
Upon `build` or `deployment`, `Gulp` will initiate any test processes as well as `Eslint` to validate and test the packages. `Gulp` will then concatenate any necessary files and minify them using `UglifyJS` into a `.min.js` file for production.