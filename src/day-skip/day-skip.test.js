import DaySkip from './day-skip'

const x = DaySkip(7, false, new Date(2017, 9, 9))

test(`Check Function Skips`, () => {
  expect(x.skips()).toEqual([0,0,0,0,0,2,2])
})

test(`Check Function Skips Total`, () => {
  expect(x.skipsTotal()).toEqual(2)
})

test(`Check Function Skips List`, () => {
  //expect(test.skipsList()).toEqual([]])
})

test(`Checks Function Days Count`, () => {
  expect(x.daysCount()).toEqual(0)
})

test(`Checks Function Sap`, () => {
  expect(x.sap()).toEqual(`20171009`)
})

test(`Counts days Between two dates and Skips Weekends Backwards`, () => {
  expect(x.skipsListSap()).toEqual([`20171009`,`20171010`,`20171011`,`20171012`,`20171013`,`20171016`, `20171017`])
})

test(`Checks Function Month Range`, () => {
  expect(x.monthRange()).toEqual({ firstDay: new Date(2017, 9, 1), lastDay: new Date(2017, 10, 0) })
})

test(`Checks Function Month Range Sap`, () => {
  expect(x.monthRangeSap()).toEqual({ firstDay: DaySkip().sap(new Date(2017, 9, 1)), lastDay: DaySkip().sap(new Date(2017, 10, 0)) })
})

test(`Checks Function Month Range Offset`, () => {
  expect(x.monthRangeOffset()).toEqual({ start: 8, end: 21, total: 30})
})

test(`Checks Sap JS`, () => {
  expect(x.sapJs(DaySkip().sap())).toEqual(new Date(2017, 9, 9))
})

test(`Checks Function Sap List`, () => {
  expect(x.sapJsList([DaySkip().sap(), DaySkip().sap()])).toEqual([new Date(2017, 9, 9), new Date(2017, 9, 9)])
})