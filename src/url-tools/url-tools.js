/**
* @author James Nicholls
* @module UrlTools
* @desc Converts URL query strings into JavaScript Objects and vice-versa
*/

/**
 * @constructor module:UrlTools
 */
const UrlTools = () => {
	let expo = {}
	/**
   * @function paramsQuery
   * @param {String} add
   * @param {Object} que
   * @desc Builds a URL query string from an object of key pair values
   * @returns {String} Returns a URL query string
   */
	expo.paramsQuery = (add, que) => {
		return que.reduce((a, q, i) => {
			let key = Object.keys(q)[0]
			a = `${a}${key}=${q[key]}&`
			a = i === que.length-1 ? a.slice(0, -1) : a
			return a
		}, `${add}?`)
	}
	/**
   * @function paramsJson
   * @param {String} que
   * @desc Builds a JavaScript object from a URL query string
   * @returns {Object} Returns a URL query object
   */
	expo.paramsJson = que => {
		let path = que.split(`?`)
		let queries = path[1].split(`&`)
		queries = queries.map(x => {
			x = x.split(`=`)
			return { [x[0]]: x[1] }
		})
		return { url: path[0], params: [...queries] }
	}	
	return expo
}

export default UrlTools

/* 
//Test Data
const test = UrlTools()
const j = JSON.parse(`{ "url": "https://49itpnp2eh.execute-api.eu-west-1.amazonaws.com/dev/granted-permits/default", "params": [ { "startIssueDate": "20170929" }, { "endIssueDate": "20171009" }, { "notifiableOrganisationName": "BARNET" }, { "noticePrefix": "MX037" } ] }`)
const p = `https://49itpnp2eh.execute-api.eu-west-1.amazonaws.com/dev/granted-permits/default?startIssueDate=20170929&endIssueDate=20171009&notifiableOrganisationName=BARNET&noticePrefix=MX037`
//Test
console.log(test.paramsQuery(j.url, j.params))
console.log(test.paramsJson(p))
 */